﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace application.Model
{
    /// <summary>
    /// class used for databind in a list of checkbox
    /// </summary>
    public class CheckBoxBinding : ViewModels.ViewModelBase
    {
        public string FilterName { get; set; }
        private bool _isChecked;
        public bool IsChecked
        {
            get
            {
                return _isChecked;
            }
            set
            {
                _isChecked = value;
                RaisePropertyChanged("IsChecked");
            }
        }

        public CheckBoxBinding(string FilterName)
        {
            this.FilterName = FilterName;
            IsChecked = false;
        }
    }
}
