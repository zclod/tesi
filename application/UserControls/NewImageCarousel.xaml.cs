﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace application.UserControls
{
    public partial class NewImageCarousel : UserControl, INotifyPropertyChanged
    {
        #region Properties
        public static DependencyProperty ImageListProperty = 
            DependencyProperty.Register(
            "ImageList", 
            typeof(ObservableCollection<string>), 
            typeof(NewImageCarousel),
            new PropertyMetadata(ImageListChangedCallback)
            );

        private static void ImageListChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var imageUrl = (ObservableCollection<string>)d.GetValue(ImageListProperty);
            _images.Clear();

            if (imageUrl != null)
            {
                foreach (var item in imageUrl)
                {
                    Image newImage = new Image();
                    newImage.Source = new BitmapImage(new Uri(item, UriKind.RelativeOrAbsolute));
                    _images.Add(newImage);
                }
            }
        }


        public ObservableCollection<string> ImageList
        {
            get
            {
                return (ObservableCollection<string>)GetValue(ImageListProperty);
            }
            set
            {
                SetValue(ImageListProperty, value);
            }
        }

        private static ObservableCollection<Image> _images;

        private int _imageIndex;
        public int ImageIndex
        {
            get
            {
                return _imageIndex;
            }
            set
            {
                _imageIndex = value;
                if(_images != null && _images.Count > 0)
                {
                    displayImage.Source = _images[_imageIndex].Source;
                }
                else
                {
                    displayImage.Source = null;
                }
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region constructor
        public NewImageCarousel()
        {
            InitializeComponent();
            _images = new ObservableCollection<Image>();
        }
        #endregion

        #region helpers
        private void addImages()
        {
            if (ImageList != null && ImageList.Count > 0)
            {
                foreach (var imageUrl in ImageList)
                {
                    Image newImage = new Image();
                    newImage.Source = new BitmapImage(new Uri(imageUrl,UriKind.RelativeOrAbsolute));

                    _images.Add(newImage);
                }
                _imageIndex = 0;
            }
        }

        private void nextImage()
        {
            if (ImageIndex < _images.Count - 1)
            {
                ImageIndex++;
            }
        }

        private void prevImage()
        {
            if (ImageIndex > 0)
            {
                ImageIndex--;
            }
        }
        #endregion

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

        #region Event Handler
        private void PrevButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            prevImage();
        }

        private void NextButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            nextImage();
        }
        #endregion
    }
}
