﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace application.ValueConverters
{
    public sealed class LinearToNonLinearScaleConverter : IValueConverter
    {
    //Let the intended Maximum value be M. 
    //Let the value at the slider midpoint be m. 
    //(obviously, 0 < m < M), then 
    //   A = - M*m^2 / (M^2 - 2*m*M)
    //   B = M*m^2 / (M^2 - 2*m*M)
    //   C = Ln((M - m)^2 / m^2)
        public static double A = -5000;
        public static double B = 5000;
        public static double C = 2.197224577;

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Math.Log(((double)value - A)/B)/C;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return A + B * Math.Exp(C * (double)value);
        }
    }
}
