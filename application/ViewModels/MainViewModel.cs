﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlacesApi;
using System.Collections.ObjectModel;
using System.Device.Location;
using Microsoft.Phone.Maps.Toolkit;
using System.Windows;
using Windows.Devices.Geolocation;
using application.Model;
using GalaSoft.MvvmLight.Command;
using Microsoft.Phone.Controls;

namespace application.ViewModels
{
    public class MainViewModel: ViewModelBase
    {

        #region Properties
        private GooglePlacesClient googleService = null;

        /// <summary>
        /// token that point to the next page of the results
        /// </summary>
        private string _nextPageToken { get; set; }

        /// <summary>
        /// list of result from the current query
        /// </summary>
        private ObservableCollection<Result> _placeResults;
        public ObservableCollection<Result> PlaceResults
        {
            get
            {
                return _placeResults;
            }
            set
            {
                _placeResults = value;
                RaisePropertyChanged("PlaceResults");
            }
        }

        /// <summary>
        /// result selected by the user
        /// </summary>
        private Result _selectedPlace;
        public Result SelectedPlace
        {
            get
            {
                return _selectedPlace;
            }
            set
            {
                _selectedPlace = value;
                RaisePropertyChanged("SelectedPlace");
            }
        }

        /// <summary>
        /// details of place selected by the user
        /// </summary>
        private PlacesApi.PlaceDetail _selectedPlaceDetail;
        public PlacesApi.PlaceDetail SelectedPlaceDetail
        {
            get
            {
                return _selectedPlaceDetail;
            }
            set
            {
                _selectedPlaceDetail = value;
                RaisePropertyChanged("SelectedPlaceDetail");
            }
        }

        /// <summary>
        /// string to query the google api
        /// </summary>
        private string _textQuery;
        public string TextQuery
        {
            get
            {
                return _textQuery;
            }
            set
            {
                _textQuery = value;
                RaisePropertyChanged("TextQuery");
            }
        }

        /// <summary>
        /// radius used in google api request
        /// </summary>
        private double _searchRadius;
        public double SearchRadius
        {
            get
            {
                return _searchRadius;
            }
            set
            {
                _searchRadius = value;
                RaisePropertyChanged("SearchRadius");
            }
        }

        private bool _useMyPosition;
        public bool UseMyPosition
        {
            get
            {
                return _useMyPosition;
            }
            set
            {
                _useMyPosition = value;
                RaisePropertyChanged("UseMyPosition");
            }
        }

        /// <summary>
        /// position to include in place search
        /// </summary>
        private GeoCoordinate _searchPosition;
        public GeoCoordinate SearchPosition
        {
            get
            {
                return _searchPosition;
            }
            set
            {
                _searchPosition = value;
                RaisePropertyChanged("SearchPosition");
            }
        }

        /// <summary>
        /// user's position
        /// </summary>
        private GeoCoordinate _currentPosition;
        public GeoCoordinate CurrentPosition
        {
            get
            {
                return _currentPosition;
            }
            set
            {
                _currentPosition = value;
                RaisePropertyChanged("CurrentPosition");
            }
        }

        /// <summary>
        /// position picked by the user on the map
        /// </summary>
        private GeoCoordinate _selectedPosition;
        public GeoCoordinate SelectedPosition
        {
            get
            {
                return _selectedPosition;
            }
            set
            {
                _selectedPosition = value;
                RaisePropertyChanged("SelectedPosition");
            }
        }

        /// <summary>
        /// list of filters used in the request
        /// </summary>
        private ObservableCollection<CheckBoxBinding> _filters;
        public ObservableCollection<CheckBoxBinding> Filters
        {
            get
            {
                return _filters;
            }
            set
            {
                _filters = value;
                RaisePropertyChanged("Filters");
            }
        }

        /// <summary>
        /// visibility control of the search panel in the master page
        /// </summary>
        private bool _isOverlayVisible;
        public bool IsOverlayVisible
        {
            get
            {
                return _isOverlayVisible;
            }
            set
            {
                _isOverlayVisible = value;
                RaisePropertyChanged("IsOverlayVisible");
            }
        }

        /// <summary>
        /// visibility control of the images carousel panel in the detail page
        /// </summary>
        private bool _isPhotoViewerVisible;
        public bool IsPhotoViewerVisible
        {
            get
            {
                return _isPhotoViewerVisible;
            }
            set
            {
                _isPhotoViewerVisible = value;
                RaisePropertyChanged("IsPhotoViewerVisible");
            }
        }

        /// <summary>
        /// true if the user had selected a location on the map
        /// </summary>
        private bool _isSelectedPosition;
        public bool IsSelectedPosition 
        {
            get
            {
                return _isSelectedPosition;
            }
            set
            {
                _isSelectedPosition = value;
                RaisePropertyChanged("IsSelectedPosition");
            }
        }

        /// <summary>
        /// Property used for binding the visibility of the progress bar
        /// </summary>
        private bool _isLoading;
        public bool IsLoading
        {
            get
            {
                return _isLoading;
            }
            set
            {
                _isLoading = value;
                RaisePropertyChanged("IsLoading");
            }
        }
        #endregion

        #region Commands

        private RelayCommand _searchCommand;
        public RelayCommand SearchCommand
        {
            get
            {
                if (_searchCommand == null)
                {
                    _searchCommand = new RelayCommand(async () => {
                        IsOverlayVisible = false;
                        IsLoading = true;
                        try
                        {
                            await this.Search();
                        }
                        catch (Exception)
                        {
                            //todo : possibile codice per lanciare le impostazioni da message box non sempre funzionante
                            //var dlg = new CustomMessageBox();
                            //dlg.Message = "ciao";
                            //dlg.LeftButtonContent = "settings";
                            //dlg.MouseLeftButtonDown += (async (sender, e) =>      
                            //{
                            //    Uri networkSettingsUri = new Uri("ms-settings-cellular:");
                            //    await Windows.System.Launcher.LaunchUriAsync(networkSettingsUri);
                            //});
                            //dlg.Show();
                            MessageBox.Show(Resources.AppResources.MessageBoxTextConnectionProblems, Resources.AppResources.Warning, new MessageBoxButton());
                            
                        }
                        IsLoading = false;
                    });
                }
                return _searchCommand;
            }
        }
        
        private RelayCommand _deleteSelectetPosition;
        public RelayCommand DeleteSelectetPosition
        {
            get
            {

                if (_deleteSelectetPosition == null)
                {
                    _deleteSelectetPosition = new RelayCommand(() =>
                    {
                        SelectedPosition = null;
                    });
                }
                return _deleteSelectetPosition;
            }
        }

        private RelayCommand _lauchTelephone;
        public RelayCommand LaunchTelephone
        {
            get
            {
                if (_lauchTelephone == null)
                {
                    _lauchTelephone = new RelayCommand(async () =>
                    {
                        Uri telUri = new Uri("tel:" + this._selectedPlaceDetail.international_phone_number);
                        await Windows.System.Launcher.LaunchUriAsync(telUri);
                    });
                }
                return _lauchTelephone;
            }
        }

        private RelayCommand _lauchBrowser;
        public RelayCommand LaunchBrowser
        {
            get
            {
                if (_lauchBrowser == null)
                {
                    _lauchBrowser = new RelayCommand(async () =>
                    {
                        if (_selectedPlaceDetail.website != null)
                        {
                            Uri siteUri = new Uri(this._selectedPlaceDetail.website);
                            await Windows.System.Launcher.LaunchUriAsync(siteUri);
                        }
                    });
                }
                return _lauchBrowser;
            }
        }

        private RelayCommand _lauchNavigator;
        public RelayCommand LaunchNavigator
        {
            get
            {
                if (_lauchNavigator == null)
                {
                    _lauchNavigator = new RelayCommand(async () =>
                    {
                        Uri placeLocusUri = new Uri("ms-drive-to:?destination.latitude=" + this._selectedPlaceDetail.geometry.location.lat +
                            "&destination.longitude=" + this._selectedPlaceDetail.geometry.location.lng + "&destination.name=" + this._selectedPlaceDetail.name);
                        await Windows.System.Launcher.LaunchUriAsync(placeLocusUri);
                    });
                }
                return _lauchNavigator;
            }
        }

        private RelayCommand _lauchNetworkSettings;
        public RelayCommand LauchNetworkSettings
        {
            get
            {
                if (_lauchNetworkSettings == null)
                {
                    _lauchNetworkSettings = new RelayCommand(async () =>
                    {
                        Uri networkSettingsUri = new Uri("ms-settings-cellular:");
                        await Windows.System.Launcher.LaunchUriAsync(networkSettingsUri);
                    });
                }
                return _lauchNetworkSettings;
            }
        }

        private RelayCommand _resetFiltersCommand;
        public RelayCommand ResetFiltersCommand
        {
            get
            {
                if (_resetFiltersCommand == null)
                {
                    _resetFiltersCommand = new RelayCommand(() =>
                    {
                        resetFilters();
                    });
                }
                return _resetFiltersCommand;
            }
        }

        #endregion

        #region Constructors

        public MainViewModel()
        {
            googleService = new GooglePlacesClient();
            PlaceResults = new ObservableCollection<Result>();
            Filters = new ObservableCollection<CheckBoxBinding>();
            SearchRadius = 2000;

            loadFilters();

            PropertyChanged += SelectedPositionChanged;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// perform a text query
        /// </summary>
        public async Task TextSearch()
        {
            PlaceResults.Clear();
            RootObject searchResult;

            if (SearchPosition == null)
            {
                searchResult = await googleService.textQuery(TextQuery, getSearchFilters());
            }
            else
            {
                searchResult = await googleService.textQuery(TextQuery, SearchPosition.Latitude, SearchPosition.Longitude, SearchRadius, getSearchFilters());
            }

            if (searchResult != null)
            {
                _nextPageToken = searchResult.next_page_token;
                foreach (Result r in searchResult.results)
                {
                    SmartResultAdd(r);
                }
            }
            else
            {
                throw new Exception("ciao");
            }
        }
        
        public async Task NearbySearch()
        {
            PlaceResults.Clear();
            RootObject searchResult = await googleService.getNearbyPlaces(SearchPosition.Latitude, SearchPosition.Longitude, SearchRadius, getSearchFilters());
            
            if (searchResult != null)
            {
                _nextPageToken = searchResult.next_page_token;
                foreach (Result r in searchResult.results)
                {
                    SmartResultAdd(r);
                }
            }
            else
            {
                throw new Exception("whoa");
            }
        }

        /// <summary>
        /// request the next page if present
        /// </summary>
        public async void GetNextPage()
        {
            if (_nextPageToken != null)
            {
                RootObject searchResult = await googleService.queryToken(_nextPageToken);
                _nextPageToken = searchResult.next_page_token;

                foreach (Result r in searchResult.results)
                {
                    SmartResultAdd(r);
                }
            }
        }

        /// <summary>
        /// send a request to obtain the detail of the selected place
        /// </summary>
        /// <returns></returns>
        public async Task getSelectedPlaceDetails()
        {//todo: se non si riesce a ricevere il risultato fare la conversione dal posto generale ai dettagli
            PlaceDetailResponse result = await googleService.getPlaceDetail(SelectedPlace.reference);
            SelectedPlaceDetail = result.result;

            SelectedPlaceDetail.photoUris = new ObservableCollection<string>();
            if (SelectedPlaceDetail.photos != null)
            {
                foreach (var photoRefernce in SelectedPlaceDetail.photos)
                {
                    //SelectedPlaceDetail.photoUris.Add(googleService.GetPhotoUri(photoRefernce.photo_reference, photoRefernce.width, photoRefernce.height));
                    SelectedPlaceDetail.photoUris.Add(googleService.GetPhotoUri(photoRefernce.photo_reference, 360, 200));
                }
            }
        }
        
        /// <summary>
        ///Method used by SearchCommand 
        /// </summary>
        /// <returns></returns>
        public async Task Search()
        {//todo : rifinire metodo search; iterazione 1.0
            //IsOverlayVisible = false;

            if (SelectedPosition != null)
            {
                SearchPosition = SelectedPosition;
            }
            else
            {
                await SetCurrentPosition();
                SearchPosition = CurrentPosition;
            }

            if (TextQuery != null && TextQuery.Length > 0)
            {
                await TextSearch();
            }
            else
            {
                await NearbySearch();
            }
        }

        #endregion

        #region Helpers

        /// <summary>
        /// set the corret GeoCoordinate to the result and add the element to the result list
        /// </summary>
        /// <param name="r"></param>
        private void SmartResultAdd(Result r)
        {
            r.coordinate = new GeoCoordinate(r.geometry.location.lat, r.geometry.location.lng);
            PlaceResults.Add(r);
        }

        /// <summary>
        /// Set the SearchPosition property to the user's current position
        /// </summary>
        public async Task SetCurrentPosition()
        {
            Geolocator locator = new Geolocator();
            Geoposition currentPosition = null;

            try
            {
                currentPosition = await locator.GetGeopositionAsync(); 
            }
            catch (UnauthorizedAccessException)
            {
                
            }

            this.CurrentPosition = currentPosition.Coordinate.ToGeoCoordinate();
        }


        private void loadFilters()
        {
            //Filters.Add(new CheckBoxBinding("food"));
            //Filters.Add(new CheckBoxBinding("bar"));
            //Filters.Add(new CheckBoxBinding("restaurant"));
            //Filters.Add(new CheckBoxBinding("other"));
            foreach (var filter in googleService.FilterList)
            {
                Filters.Add(new CheckBoxBinding(filter));
            }
        }

        private void resetFilters()
        {
            foreach (var item in Filters)
            {
                item.IsChecked = false;
            }
        }

        private string[] getSearchFilters()
        {

            List<string> result = new List<string>();
            foreach (var item in Filters)
            {
                if (item.IsChecked)
                {
                    result.Add(item.FilterName);
                }
            }

            if (result.Count != 0)
            {
                return result.ToArray();
            }

            return null;
        }

        #endregion

        #region Obsolete methods

        [Obsolete]
        public async void NearbySearch(double latitude, double longitude, double radius, params string[] types)
        {
            PlaceResults.Clear();
            RootObject searchResult = await googleService.getNearbyPlaces(latitude, longitude, radius, types);
            _nextPageToken = searchResult.next_page_token;

            foreach (Result r in searchResult.results)
            {
                SmartResultAdd(r);
            }
        }

        [Obsolete]
        public async void TextSearch1()
        {
            PlaceResults.Clear();
            RootObject searchResult = await googleService.textQuery(TextQuery);
            _nextPageToken = searchResult.next_page_token;

            foreach (Result r in searchResult.results)
            {
                SmartResultAdd(r);
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// update the visibility expression IsSelectedPlace when SelectedPosition changes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SelectedPositionChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedPosition")
            {
                this.IsSelectedPosition = (this.SelectedPosition != null);
            }
        }

        #endregion
    }
}
