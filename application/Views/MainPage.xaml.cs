﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using application.Resources;
using PlacesApi;
using application.ViewModels;
using System.Threading.Tasks;
using Microsoft.Phone.Maps.Controls;
using System.Collections.ObjectModel;
using Microsoft.Phone.Maps.Toolkit;
using Microsoft.Phone.Maps.Services;
using System.Device.Location;

namespace application
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            this.DataContext = App.PlacesVM;
            Loaded += MainPage_Loaded;
            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }
        private MainViewModel _placeVM { get { return this.DataContext as MainViewModel; } }

        #region Event Handlers

        async void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            //set the list of pushpin for data binding
            ObservableCollection<DependencyObject> childern = MapExtensions.GetChildren(MyMap);
            var obj = childern.FirstOrDefault(x => x.GetType() == typeof(MapItemsControl)) as MapItemsControl;

            obj.ItemsSource = _placeVM.PlaceResults;

            //center the map on current position
            await _placeVM.SetCurrentPosition();
            ZoomOnPosition(_placeVM.CurrentPosition);
        }

        //handle the hardware back button press
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (!_placeVM.IsOverlayVisible)
            {
                if (PivotControl.SelectedIndex == 1)
                {
                    e.Cancel = true;
                    PivotControl.SelectedIndex = 0;
                }
                else
                {
                    base.OnBackKeyPress(e);
                }
            }
            else
            {
                e.Cancel = true;
                _placeVM.IsOverlayVisible = false;
                
            }
        }  


        private void SearchBarButton_Click(object sender, System.EventArgs e)
        {
            _placeVM.IsOverlayVisible = true;
        }

        private void ResultList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            //_placeVM.SelectedPlace = e.OriginalSource as Result;
            //_placeVM.SelectedPlace = ((FrameworkElement)e.OriginalSource).DataContext as Result;
            var element = (LongListSelector)sender;
            _placeVM.SelectedPlace = (Result)element.SelectedItem;
            //await _placeVM.getSelectedPlaceDetails();        

            //NavigationService.Navigate(new Uri("/Views/PlaceDetail.xaml", UriKind.Relative));
        }

        private async void ResultList_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //_placeVM.SelectedPlace = ((FrameworkElement)e.OriginalSource).DataContext as Result;
            //_placeVM.getSelectedPlaceDetails();
            //NavigationService.Navigate(new Uri("/Views/PlaceDetail.xaml"));
            //var element = (LongListSelector)sender;
            //_placeVM.SelectedPlace = (Result)element.SelectedItem;
            //_placeVM.getSelectedPlaceDetails();
            await _placeVM.getSelectedPlaceDetails();
            NavigationService.Navigate(new Uri("/Views/PlaceDetail.xaml", UriKind.Relative));
        }

        private void Pushpin_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            var selected = (sender as Pushpin).DataContext as Result;
            PivotControl.SelectedIndex = 1;
            ResultList.ScrollTo(selected);
            ResultList.SelectedItem = selected;
        }

        private void MyMap_Hold(object sender, System.Windows.Input.GestureEventArgs e)
        {
            _placeVM.SelectedPosition = MyMap.ConvertViewportPointToGeoCoordinate(e.GetPosition((UIElement)sender));
        }

        #endregion

        #region helpers

        private void ZoomOnPosition(GeoCoordinate target)
        {
            this.MyMap.Center = target;
            this.MyMap.ZoomLevel = 15;
        }

        #endregion
        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}