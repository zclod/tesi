﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using application.ViewModels;

namespace application
{
    public partial class PlaceDetail : PhoneApplicationPage
    {
        public PlaceDetail()
        {
            InitializeComponent();
            this.DataContext = App.PlacesVM;
        }

        private MainViewModel _placeVM { get { return this.DataContext as MainViewModel; } }


        #region Event Handlers
        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            _placeVM.IsPhotoViewerVisible = true;
            MyCarousel.ImageIndex = 0;
        }

        //handle the hardware back button press
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (!_placeVM.IsPhotoViewerVisible)
            {
                base.OnBackKeyPress(e);
            }
            else
            {
                e.Cancel = true;
                _placeVM.IsPhotoViewerVisible = false;
            }
        }
        #endregion
    }
}