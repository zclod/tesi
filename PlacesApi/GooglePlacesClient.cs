﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;

namespace PlacesApi
{
    public class GooglePlacesClient
    {
        #region default values

        private const int DEFAULT_RADIUS = 5000;
        private const bool DEFAULT_SENSOR = true;

        #endregion

        #region Propreties

        /// <summary>
        /// API key to retrive data from google
        /// </summary>
        private const String API_KEY = "AIzaSyDw5sNRyOLOBoPYdO-OUJMHowuIRL_Qjco";
        
        /// <summary>
        /// Nearby Places Search
        /// Required parameters
        ///•key — Your application's API key. This key identifies your application for purposes of quota management and so that Places added from your application are made immediately available to your app. Visit the APIs Console to create an API Project and obtain your key.
        ///•location — The latitude/longitude around which to retrieve Place information. This must be specified as latitude,longitude.
        ///•radius — Defines the distance (in meters) within which to return Place results. The maximum allowed radius is 50 000 meters. Note that radius must not be included if rankby=distance (described under Optional parameters below) is specified.
        ///•sensor — Indicates whether or not the Place request came from a device using a location sensor (e.g. a GPS) to determine the location sent in this request. This value must be either true or false.

        ///Optional parameters
        ///•keyword — A term to be matched against all content that Google has indexed for this Place, including but not limited to name, type, and address, as well as customer reviews and other third-party content.
        ///•language — The language code, indicating in which language the results should be returned, if possible. See the list of supported languages and their codes. Note that we often update supported languages so this list may not be exhaustive.
        ///•minprice and maxprice (optional) — Restricts results to only those places within the specified range. Valid values range between 0 (most affordable) to 4 (most expensive), inclusive. The exact amount indicated by a specific value will vary from region to region.
        ///•name — One or more terms to be matched against the names of Places, separated with a space character. Results will be restricted to those containing the passed name values. Note that a Place may have additional names associated with it, beyond its listed name. The API will try to match the passed name value against all of these names; as a result, Places may be returned in the results whose listed names do not match the search term, but whose associated names do.
        ///•opennow — Returns only those Places that are open for business at the time the query is sent. Places that do not specify opening hours in the Google Places database will not be returned if you include this parameter in your query.
        ///•rankby — Specifies the order in which results are listed. Possible values are: ◦prominence (default). This option sorts results based on their importance. Ranking will favor prominent places within the specified area. Prominence can be affected by a Place's ranking in Google's index, the number of check-ins from your application, global popularity, and other factors.
        ///◦distance. This option sorts results in ascending order by their distance from the specified location. When distance is specified, one or more of keyword, name, or types is required.
        ///•types — Restricts the results to Places matching at least one of the specified types. Types should be separated with a pipe symbol (type1|type2|etc). See the list of supported types.
        ///•pagetoken — Returns the next 20 results from a previously run search. Setting a pagetoken parameter will execute a search with the same parameters used previously — all parameters other than pagetoken will be ignored. 
        ///•zagatselected — Add this parameter (just the parameter name, with no associated value) to restrict your search to locations that are Zagat selected businesses. This parameter must not include a true or false value. The zagatselected parameter is experimental, and is only available to Places API enterprise customers        
        /// </summary>
        private const String NEARBY_PLACES = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
        
        /// <summary>
        /// Text Query
        /// Required parameters
        ///•query — The text string on which to search, for example: "restaurant". The Place service will return candidate matches based on this string and order the results based on their perceived relevance.
        ///•key — Your application's API key. This key identifies your application for purposes of quota management and so that Places added from your application are made immediately available to your app. Visit the APIs Console to create an API Project and obtain your key.
        ///•sensor — Indicates whether or not the Place request came from a device using a location sensor (e.g. a GPS) to determine the location sent in this request. This value must be either true or false.

        ///Optional parameters
        ///•location — The latitude/longitude around which to retrieve Place information. This must be specified as latitude,longitude. If you specify a location parameter, you must also specify a radius parameter.
        ///•radius — Defines the distance (in meters) within which to bias Place results. The maximum allowed radius is 50 000 meters. Results inside of this region will be ranked higher than results outside of the search circle; however, prominent results from outside of the search radius may be included.
        ///•language — The language code, indicating in which language the results should be returned, if possible. See the list of supported languages and their codes. Note that we often update supported languages so this list may not be exhaustive.
        ///•minprice and maxprice (optional) — Restricts results to only those places within the specified price level. Valid values are in the range from 0 (most affordable) to 4 (most expensive), inclusive. The exact amount indicated by a specific value will vary from region to region.
        ///•opennow — Returns only those Places that are open for business at the time the query is sent. Places that do not specify opening hours in the Google Places database will not be returned if you include this parameter in your query.
        ///•types — Restricts the results to Places matching at least one of the specified types. Types should be separated with a pipe symbol (type1|type2|etc). See the list of supported types.
        ///•zagatselected — Add this parameter (just the parameter name, with no associated value) to restrict your search to locations that are Zagat selected businesses. This parameter must not include a true or false value. The zagatselected parameter is experimental, and is only available to Places API enterprise customers.
        /// </summary>
        private const String TEXT_QUERY = "https://maps.googleapis.com/maps/api/place/textsearch/json";

        /// <summary>
        /// required parameters:
        /// •key — Your application's API key. This key identifies your application for purposes of quota management. Visit the APIs Console to create an API Project and obtain your key.
        /// •photoreference — A string identifier that uniquely identifies a photo. Photo references are returned from either a Place Search or Place Details request.
        /// •sensor — Indicates whether or not the Place Photo request came from a device using a location sensor (e.g. a GPS). This value must be either true or false.
        /// •maxheight or maxwidth — Specifies the maximum desired height or width, in pixels, of the image returned by the Place Photos service. If the image is smaller than the values specified, the original image will be returned. If the image is larger in either dimension, it will be scaled to match the smaller of the two dimensions, restricted to its original aspect ratio. Both the maxheight and maxwidth properties accept an integer between 1 and 1600.
        /// </summary>
        private const String PHOTO_REQUEST = "https://maps.googleapis.com/maps/api/place/photo";

        /// <summary>
        /// •key (required) — Your application's API key. This key identifies your application for purposes of quota management and so that Places added from your application are made immediately available to your app. Visit the APIs Console to create an API Project and obtain your key.
        /// •reference (required) — A textual identifier that uniquely identifies a place, returned from a Place search.
        /// •sensor (required) — Indicates whether or not the Place Details request came from a device using a location sensor (e.g. a GPS). This value must be either true or false.
        /// </summary>
        private const String PLACE_DETAIL = "https://maps.googleapis.com/maps/api/place/details/json";

        /// <summary>
        /// http client to perform rest operation
        /// </summary>
        private RestClient client;

        public List<string> FilterList { get; private set; }
        #endregion

        #region Constructors

        /// <summary>
        /// default constructor
        /// </summary>
        public GooglePlacesClient()
        {
            client = new RestClient();
            LoadFilterStrings();
        }

        #endregion

        #region obsolete methods
        //object returned by the google api
        [Obsolete]
        public RootObject Data { get; private set; }

        [Obsolete]
        public void query(string endpoint, string latitude, string longitude, string queryString, string radius, bool sensor)
        {
            var request = new RestRequest(endpoint, Method.GET);
            request.AddParameter("key", API_KEY);
            request.AddParameter("sensor", "true");

            if (latitude != null && longitude != null)
            {
                request.AddParameter("location", latitude + "," + longitude);
            }

            if (queryString != null)
            {
                request.AddParameter("query", queryString.Replace(" ", "+"));
            }

            if (radius != null)
            {
                request.AddParameter("radius", radius);
            }

            client.ExecuteAsync<RootObject>(request, asyncCallback);
        }

        private void asyncCallback(IRestResponse<RootObject> arg1, RestRequestAsyncHandle arg2)
        {
            Data = arg1.Data;
        }
        [Obsolete]
        public Task<RootObject> queryAsync(string endpoint, string latitude, string longitude, string queryString, string radius, bool sensor)
        {
            var request = new RestRequest(endpoint, Method.GET);
            request.AddParameter("key", API_KEY);
            request.AddParameter("sensor", "true");

            if (latitude != null && longitude != null)
            {
                request.AddParameter("location", latitude + "," + longitude);
            }

            if (queryString != null)
            {
                request.AddParameter("query", queryString.Replace(" ", "+"));
            }

            if (radius != null)
            {
                request.AddParameter("radius", radius);
            }

            var tcs = new TaskCompletionSource<RootObject>();
            client.ExecuteAsync<RootObject>(request, response => {
                tcs.SetResult(response.Data);
            });

            return tcs.Task;
        }
        #endregion

        #region Private methods
        /*
         *base method for queryng the google place api asyncronously, add the API_KEY to the provided request 
         */
        private Task<RootObject> queryAsync(RestRequest request)
        {
            
            request.AddParameter("key", API_KEY);
            
            var tcs = new TaskCompletionSource<RootObject>();
            client.ExecuteAsync<RootObject>(request, response =>
            {
                tcs.SetResult(response.Data);
            });

            return tcs.Task;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// get up to additiona values from the previous query
        /// </summary>
        /// <param name="token">token that reference the next page of results</param>
        /// <returns>return the <see cref="RootObject"/> from the google api</returns>
        public Task<RootObject> queryToken(string token)
        {
            var request = new RestRequest(NEARBY_PLACES, Method.GET);
            request.AddParameter("pagetoken", token);
            request.AddParameter("sensor", DEFAULT_SENSOR.ToString().ToLower());

            return queryAsync(request);
        }


        public Task<String> requestPhoto(string photoReference, int maxWidth, int maxHeight)
        {
            var request = new RestRequest(PHOTO_REQUEST, Method.GET);
            request.AddParameter("key", API_KEY);
            request.AddParameter("photoreference", photoReference);
            request.AddParameter("maxwidth", maxWidth);
            request.AddParameter("maxheight", maxHeight);

            //StringBuilder builder = new StringBuilder(PHOTO_REQUEST + "?key=" + API_KEY + "&photoreference=" + photoReference + "&maxwidth=" + maxWidth.ToString() + "&maxheight=" + maxHeight.ToString());

            var tcs = new TaskCompletionSource<String>();
            client.ExecuteAsync(request, response =>
            {
                tcs.SetResult(response.Content);
            });

            return tcs.Task;
        }

            #region getNearbyPlaces

        /// <summary>
        /// base method to request nearby places throught the google api
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="radius"></param>
        /// <param name="sensor"></param>
        /// <param name="keyword"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public Task<RootObject> getNearbyPlaces(double latitude, double longitude, double radius, bool sensor, string keyword, params string[] types)
        {
            var request = new RestRequest(NEARBY_PLACES, Method.GET);
            request.AddParameter("location", latitude + "," + longitude);
            request.AddParameter("sensor", sensor.ToString().ToLower());
            if (radius != 0)
            {
                request.AddParameter("radius", radius);
            }
            else
            {
                request.AddParameter("radius", DEFAULT_RADIUS);
            }

            if (keyword != null)
            {
                request.AddParameter("keyword", keyword);
            }
            if (types != null)
            {
                request.AddParameter("types", addTypes(types));
            }

            return queryAsync(request);
        }
        
        public Task<RootObject> getNearbyPlaces(double latitude, double longitude, double radius)
        {
            return getNearbyPlaces(latitude, longitude, radius, DEFAULT_SENSOR, null, null);
        }

        public Task<RootObject> getNearbyPlaces(double latitude, double longitude, double radius, params string[] types)
        {
            return getNearbyPlaces(latitude, longitude, radius, DEFAULT_SENSOR, null, types);
        }

        public Task<RootObject> getNearbyPlaces(double latitude, double longitude)
        {
            return getNearbyPlaces(latitude, longitude, DEFAULT_RADIUS, DEFAULT_SENSOR, null, null);
        }

        public Task<RootObject> getNearbyPlaces(double latitude, double longitude, params string[] types)
        {
            return getNearbyPlaces(latitude, longitude, DEFAULT_RADIUS, DEFAULT_SENSOR, null, types);
        }

            #endregion

            #region textQuery
        public Task<RootObject> textQuery(string query, bool sensor, double latitude, double longitude, double radius, params string[] types)
        {
            var request = new RestRequest(TEXT_QUERY, Method.GET);
            request.AddParameter("query", query);
            request.AddParameter("location", latitude + "," + longitude);
            request.AddParameter("sensor", sensor.ToString().ToLower());
            if (radius != 0)
            {
                request.AddParameter("radius", radius);
            }
            else
            {
                request.AddParameter("radius", DEFAULT_RADIUS);
            }

            if (types != null)
            {
                request.AddParameter("types", addTypes(types));
            }

            return queryAsync(request);
        }

        public Task<RootObject> textQuery(string query, double latitude, double longitude, double radius, params string[] types)
        {
            return textQuery(query, DEFAULT_SENSOR, latitude, longitude, radius, types);
        }

        public Task<RootObject> textQuery(string query, double latitude, double longitude, params string[] types)
        {
            return textQuery(query, DEFAULT_SENSOR, latitude, longitude, DEFAULT_RADIUS, types);
        }

        public Task<RootObject> textQuery(string query, double latitude, double longitude, double radius)
        {
            return textQuery(query, DEFAULT_SENSOR, latitude, longitude, radius, null);
        }

        public Task<RootObject> textQuery(string query, double latitude, double longitude)
        {
            return textQuery(query, DEFAULT_SENSOR, latitude, longitude, DEFAULT_RADIUS, null);
        }

        public Task<RootObject> textQuery(string query, params string[] types)
        {
            var request = new RestRequest(TEXT_QUERY, Method.GET);
            request.AddParameter("query", query);
            request.AddParameter("sensor", DEFAULT_SENSOR.ToString().ToLower());
            if (types != null)
            {
                request.AddParameter("types", addTypes(types));
            }

            return queryAsync(request);
        }
        public Task<RootObject> textQuery(string query)
        {
            return textQuery(query, null);
        }
            #endregion

        /// <summary>
        /// get the detail about a selected place
        /// </summary>
        /// <param name="reference">unnique reference that identify the place</param>
        /// <returns>rasponse containing the detail <see cref="PlaceDetailResponse"/></returns>
        public Task<PlaceDetailResponse> getPlaceDetail(string reference)
        {
            var request = new RestRequest(PLACE_DETAIL, Method.GET);
            request.AddParameter("key", API_KEY);
            request.AddParameter("sensor", DEFAULT_SENSOR.ToString().ToLower());
            request.AddParameter("reference", reference);

            var tcs = new TaskCompletionSource<PlaceDetailResponse>();
            client.ExecuteAsync<PlaceDetailResponse>(request, response =>
            {
                tcs.SetResult(response.Data);
            });

            return tcs.Task;
        }

        #endregion

        #region Helpers

        /// <summary>
        /// generate an appropriate string containing the given values to be passed to the api
        /// </summary>
        /// <param name="types">types to insert in the request</param>
        /// <returns>string to be passed as type string in the queryes</returns>
        private string addTypes(params string[] types)
        {
            StringBuilder typesResult = new StringBuilder();

            for (int i = 0; i < types.Length; i++)
            {
                typesResult.Append(types[i]);
                if (i != types.Length - 1)
                {
                    typesResult.Append("|");
                }
            }

            return typesResult.ToString();
        }

        public string GetPhotoUri(string reference, int maxWidth, int maxHeight)
        {
            StringBuilder photoUri = new StringBuilder();
            photoUri.Append(PHOTO_REQUEST);
            photoUri.Append("?key=" + API_KEY);
            photoUri.Append("&photoreference=" + reference);
            photoUri.Append("&sensor=true");
            photoUri.Append("&maxwidth=" + maxWidth);
            photoUri.Append("&maxheight=" + maxHeight);

            return photoUri.ToString();
        }

        private void LoadFilterStrings()
        {
            FilterList = XmlParser.Parser.ParseFilters();
        }
        #endregion

#if DEBUG
        #region DEBUG wrapper
        
        public string DEBUGWrapperAddTypes(params string[] types)
        {
            return addTypes(types);
        }

        #endregion
#endif
    }
}
