﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace PlacesApi
{
    [XmlRoot("root")]
    public class Types
    {
        [XmlArray("Types")]
        [XmlArrayItem("Item")]
        public List<string> filters { get; set; }
    }

    public class XmlParser
    {
        private Types _filtersTypes;
        private const string _typesFile = "Resources/types.xml";
        private FileStream _xmlFile;
        private static XmlParser _parser;
        private XmlParser()
        {
            _xmlFile = new FileStream(_typesFile, FileMode.Open);
        }
        public static XmlParser Parser
        {
            get
            {
                if (_parser == null)
                {
                    _parser = new XmlParser();
                }
                return _parser;
            }
        }

        public List<string> ParseFilters()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Types));
                _filtersTypes = (Types)serializer.Deserialize(_xmlFile);
                return _filtersTypes.filters;
            }
            catch
            {
                
            }
            return null;
        }
    }
}
