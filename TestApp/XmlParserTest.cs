﻿using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using PlacesApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    [TestClass]
    public class XmlParserTest
    {
        [TestMethod]
        public void ParseFilterTest()
        {
            List<string> result = XmlParser.Parser.ParseFilters();
            if (result.Count == 0)
                Assert.Fail();
        }
    }
}
