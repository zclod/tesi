﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using PlacesApi;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace TestApp
{
    [TestClass]
    public class GooglePlacesClientTest
    {
        [TestMethod]
        public async Task queryTest1()
        {
            GooglePlacesClient client = new GooglePlacesClient();
            Task<RootObject> resultTask = client.queryAsync("https://maps.googleapis.com/maps/api/place/nearbysearch/json", "-33.8670522", "151.1957362", null, "1000", true);

            RootObject result = await resultTask;

            if (result == null && result.status != "OK")
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public async Task nearbyPlacesTest()
        {
            GooglePlacesClient client = new GooglePlacesClient();

            RootObject result1 = await client.getNearbyPlaces(44.6752, 11.0358, 1000, true, "food");
            RootObject result = await client.getNearbyPlaces(-33.8670522, 151.1957362, 1000, true, "cafe", "food");
            if (result == null && result.status != "OK")
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public async Task textQueryTest()
        {
            GooglePlacesClient client = new GooglePlacesClient();

            RootObject result = await client.textQuery("pizzeria nonantola");
            if (result == null && result.status != "OK")
            {
                Assert.Fail();
            }

            result = await client.textQuery("nonantola", "food", "restaurant", "bar");
            if (result == null && result.status != "OK")
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public async Task textQueryTest1()
        {
            GooglePlacesClient client = new GooglePlacesClient();

            RootObject result = await client.textQuery("nonantola");
            if (result == null && result.status != "OK")
            {
                Assert.Fail();
            }

            result = await client.getNearbyPlaces(result.results[0].geometry.location.lat, result.results[0].geometry.location.lng, "food");
            if (result == null && result.status != "OK")
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public async Task queryTokenTest()
        {
            GooglePlacesClient client = new GooglePlacesClient();
            RootObject result = await client.getNearbyPlaces(-33.8670522, 151.1957362, 1000);
            result = await client.queryToken(result.next_page_token);

            if (result == null && result.status != "OK")
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public async Task photoReuestTest()
        {
            GooglePlacesClient client = new GooglePlacesClient();
            //RootObject result = await client.getNearbyPlaces(-33.8670522, 151.1957362, 1000);
            var image = await client.requestPhoto("CnRnAAAAe-KnEqJe0niEOjkHwrcTG94skzAgqywyhw-5UTA5cr4pRGz2kIe_EzL5RTKWAWThfWetUE47vyPj47PPO_NDw1wY31CxTjNRxB_vipe0Um8-IV_f1ywaWz8O14lTCX_bb8StDe2t4IKgcOW2KRJ4ShIQ7q80PKVUBwXXaB1-F5O4whoUG7X7Vick_Ls6Y6LepfbeCvan-Nc", 100, 100);
            //result.results[1].photos[0].photo_reference
            if (image == null)
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        public void addTypesTest()
        {
            GooglePlacesClient client = new GooglePlacesClient();
            string s = client.DEBUGWrapperAddTypes("ciao", "cacca", "pupu", "merda");

            if (s.CompareTo("ciao|cacca|pupu|merda") != 0)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public async Task PlaceDetailTest()
        {
            GooglePlacesClient client = new GooglePlacesClient();
            RootObject result = await client.textQuery("pizzeria nonantola");

            var detail = await client.getPlaceDetail(result.results[0].reference);
            if (detail == null && detail.status != "OK")
            {
                Assert.Fail();
            }
        }
    }
}
